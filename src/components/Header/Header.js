import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { Router, Scene, Actions } from 'react-native-router-flux'
import VewList from '../viewList/viewList';
import CreateItem from '../CreateItem/CreateItem';
import EditItem from '../EditItem/EditItem';
import DeleteItem from '../DeleteItem/DeleteItem';
import ListFilterItems from '../ListItems/ListFilterItems';
export default class Header extends React.Component {

  constructor(props){
    super(props);
  }

  componentWillMount() {
    // var firebaseConfig = {
    //   apiKey: "AIzaSyB3QJb1lDpDN6Tj7a3xy0VqLBInq5Vleps",
    //   authDomain: "test-cinte-firebase.firebaseapp.com",
    //   databaseURL: "https://test-cinte-firebase.firebaseio.com",
    //   projectId: "test-cinte-firebase",
    //   storageBucket: "",
    //   messagingSenderId: "439359676252",
    //   appId: "1:439359676252:web:efaf7b00a193c4de"
    // };
    // // Initialize Firebase
    // firebase.initializeApp(firebaseConfig);
}
  render(){

    return(
    <View style={{flex: 1, paddingTop:20}} onPress={()=> Actions.about()}>
      <Router>
      <Scene key = "root" style={styles.container} onPress={()=> Actions.about()}> 
         <Scene 
            style={styles.header}
            key = "home" 
            component = {VewList} 
            title = "Home" 
            onRight={()=> Actions.CrearItem()}
            rightTitle="Crear Item"
            initial = {true} />
         <Scene 
            key = "CrearItem" 
            onRight={()=> Actions.EditItem()}
            component = {CreateItem} 
            rightTitle="Editar Item"
            title = "Crear Item" />
         <Scene 
            key = "EditItem" 
            component = {EditItem} 
            onRight={()=> Actions.DeleteItem()}
            rightTitle="Eliminar Items"
            title = "Editar Item" />
            <Scene 
               key = "DeleteItem" 
               component = {DeleteItem} 
               onRight={()=> Actions.ListFilterItems()}
               rightTitle="Lista Items"
               title = "Eliminar Item" />
          <Scene 
            key = "ListFilterItems" 
            component = {ListFilterItems} 
            title = "Lista Items" />   
      </Scene>
      </Router>
    </View>
      
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFF"
  },  
  header: {
    backgroundColor: "#108EE9",
  },
});