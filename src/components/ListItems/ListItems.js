import React from 'react';
import { FlatList, ActivityIndicator, Text, View,StyleSheet  } from 'react-native';
import { db } from '../Config/Config';

export default class ListItems extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  async componentDidMount() {
    let itemsRef =  await db.ref('/todos');
    // const originalList = await fetch('https://jsonplaceholder.typicode.com/todos')
    // const data = await originalList.json()
    await itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ isLoading: false,
        dataSource:items });
    });
  }

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, paddingTop:20}} >
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => <Text style={styles.title} >{item.id}, {item.title}</Text>}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'left'
  },
});