import React from 'react';
import { FlatList, ActivityIndicator, Text, View,StyleSheet ,TextInput } from 'react-native';
import { db } from '../Config/Config';

export default class ListItems extends React.Component {

  constructor(props){
    super(props);
    this.state = { isLoading: true, id: -1, title: '' }
  }
  handleChangeId = e => {
    this.setState({
      id: e.nativeEvent.text
    });
  };
  handleChangeTitle = e => {
    this.setState({
      title: e.nativeEvent.text.toLowerCase()
    });
  };

  async componentDidMount() {
    let itemsRef =  await db.ref('/todos');
    await itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ isLoading: false,
        dataSource:items });
    });
  }
  
  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    let textFilter = this.state.title
    let idFilter = this.state.id
    let todos = this.state.dataSource.filter(function (item) {
      return ((item.title.toLowerCase().search(
        textFilter) !== -1));
    });
    if ((idFilter !== '') && (idFilter !== -1)) {
      todos = todos.filter(function (item) {
        if ((idFilter == item.id)) {
          return true
        } else {
          return false
        }
      });
    }
    return(
      <View style={{flex: 1, paddingTop:20}} >
        <TextInput
          placeholder='Id'
          style={styles.itemInput}
          onChange={this.handleChangeId} />
        <TextInput
          placeholder='Title'
          style={styles.itemInput}
          onChange={this.handleChangeTitle} />
        <FlatList
          data={todos}
          renderItem={({item}) => <Text style={styles.title} >{item.id}, {item.title}</Text>}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#6565fc'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'black'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});