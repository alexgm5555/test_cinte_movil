import React from 'react';
import ListItems from '../ListItems/ListItems';
import { _ } from 'lodash';
import { TouchableHighlight,
  StyleSheet,
  TextInput,
  Alert,
  AlertIOS, ActivityIndicator, Text, View  } from 'react-native';
  import { db } from '../Config/Config';

    const  deleteItem = item => {
       try {
        const itemsRef =  db.ref('/todos');
        itemsRef.once('value', snapshot => {
            let data = snapshot.val();
            var itemMap = _.findKey(data, function(data2) {
                return data2.id===item.id
            });
            if((itemMap!==undefined)&&(itemMap!==null)&&(itemMap!=='')){
                db.ref('/todos/'+itemMap).remove();
            }else{
            }
          });
       } catch (error) {
       }
    
  };

export default class deleteItems extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  async componentDidMount() {
      this.setState({ isLoading: false})
  }
  handleChangeId = e => {
    this.setState({
      id: e.nativeEvent.text
    });
  };
  handleChangeTitle = e => {
    this.setState({
      title: e.nativeEvent.text
    });
  };
  handleSubmit = () => {
    deleteItem(this.state);
  };
  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1}} >
        <TextInput
            placeholder='Id' 
            style={styles.itemInput} 
            onChange={this.handleChangeId} />
        <TextInput 
            placeholder='Title' 
            style={styles.itemInput} 
            onChange={this.handleChangeTitle} />    
        <TouchableHighlight
          style={styles.button}
          underlayColor="white"
          onPress={this.handleSubmit}
        >
          <Text style={styles.buttonText}>Eliminar</Text>
        </TouchableHighlight>
        <ListItems/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#6565fc'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'black'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});