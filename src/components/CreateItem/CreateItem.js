import React from 'react';
import { TouchableHighlight,
  StyleSheet,
  TextInput,
  AlertIOS, ActivityIndicator, Text, View  } from 'react-native';
  import { db } from '../Config/Config';

  let addItem = item => {
    db.ref('/todos').push({
      id: item.id,
      title: item.title
    });
  };

import ListItems from '../ListItems/ListItems';
export default class createItem extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  async componentDidMount() {
    // const originalList = await fetch('https://jsonplaceholder.typicode.com/todos')
    // const data = await originalList.json()
    // this.setState({ isLoading: false,
    //   dataSource:data})
      this.setState({ isLoading: false})
  }
  handleChangeId = e => {
    this.setState({
      id: e.nativeEvent.text
    });
  };
  handleChangeTitle = e => {
    this.setState({
      title: e.nativeEvent.text
    });
  };
  handleSubmit = () => {
    addItem(this.state);
  };
  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1}} >
        <TextInput
            placeholder='Id' 
            style={styles.itemInput} 
            onChange={this.handleChangeId} />
        <TextInput 
            placeholder='Title' 
            style={styles.itemInput} 
            onChange={this.handleChangeTitle} />    
        <TouchableHighlight
          style={styles.button}
          underlayColor="white"
          onPress={this.handleSubmit}
        >
          <Text style={styles.buttonText}>Guardar</Text>
        </TouchableHighlight>
        <ListItems/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#6565fc'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'black'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});